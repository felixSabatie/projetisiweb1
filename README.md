# Projet web ISI1
## Précisions

### Prérequis
La configuration de la base de donnée se fait dans le fichier : `public/includes/php/functions.php`  
Afin de pouvoir crypter les mots de passes en md5, la base de données doit être modifiée de la manière suivante :
* Passer le champ __"mdp"__ de __"Comptable"__ de __20__ à __32__ caractères
* Passer le champ __"mdp"__ de __"Visiteur"__ de __20__ à __32__ caractères

Le script de cryptage des mots de passe se situe au chemin suivant : `/public/BDD/hashPasswords.php`

### Structure du projet

Toutes les pages nécessaires au fonctionnement du site sont disposées dans le dossier `/public`.  
Seules les pages principales sont à la racine du dossier, toutes les pages php partielles se trouvent dans le dossier `/public/includes/php`, 
ou dans le dossier `/public/includes/ajax` dans le cas des pages appelées par les requêtes ajax.

### Utilisation de l'ajax

J'ai utilisé des requêtes ajax pour certains formulaire :  
#### Requêtes avec ajax :
* Saisie des frais médicaux
    * Ajout de frais hors forfait (le frais n'est pas inséré directement dans le tableau récapitulatif, je n'ai pas eu le temps de créer une template remplie en javascript. Il faut rafraîchir la page pour voir les modifications)
    * Suppression de frais hors forfait
* Validation fiches des frais
    * Validation des frais  

#### Requêtes sans ajax :
* Saisie des frais médicaux
    * Ajout de frais forfaitaires
* Validation fiches des frais
    * Reboursement des frais
* Page de connexion

### Gestion de la "modal"
Il n'y a qu'une seule "modal" bootstrap par page au maximum. Sa template vide se situe au chemin suivant : `/public/php/modal_structure.php`.  
Afin de modifier son contenu en fonction de la modal à afficher, il faut renseigner trois div voisines du bouton d'ouverture de la modal :
* `<div class="popin-title hidden">`
* `<div class="popin-content hidden">`
* `<div class="popin-footer hidden">`

Ces div seront ensuite insérées dans la modal à leur place corrsepondante grâce au script stocké au chemin suivant : `/public/js/gestion_modal.js`. 
Cela permet d'éviter d'alourdir la page avec de nombreuses modal, qui peuvent également poser des problèmes de conflits et d'affichage.