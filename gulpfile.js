var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "projetisiweb.dev"
    });
});

gulp.task('sass', function () {
    return gulp.src('ressource/scss/**/*.scss')
                .pipe(sass())
                .on('error', function(err){
            		console.log(err.toString());
					this.emit('end');
                })
                .pipe(concat('master.css'))
                .pipe(gulp.dest('./public/includes/css/'))
                .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch("ressource/scss/**/*.scss", ['sass']).on('change', browserSync.reload);
    gulp.watch(["**/*.html", "**/*.php"]).on('change', browserSync.reload);
});
