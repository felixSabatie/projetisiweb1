<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/functions.php');

try {
    $pdo = getDb();

    // Script pour les visiteurs
    $request = "
    SELECT id, mdp
    FROM Visiteur
    ";

    $statement = $pdo->prepare($request);
    $statement->execute();

    while($row = $statement->fetch()) {
        $request = "
        UPDATE Visiteur
        SET mdp = :mdp
        WHERE id = :id
        ";

        $updateStatement = $pdo->prepare($request);
        $updateStatement->bindParam(':mdp', md5($row['mdp']));
        $updateStatement->bindParam(':id', $row['id']);
        $updateStatement->execute();
    }

    // Script pour les comptables
    $request = "
    SELECT id, mdp
    FROM Comptable
    ";

    $statement = $pdo->prepare($request);
    $statement->execute();

    while($row = $statement->fetch()) {
        $request = "
        UPDATE Comptable
        SET mdp = :mdp
        WHERE id = :id
        ";

        $updateStatement = $pdo->prepare($request);
        $updateStatement->bindParam(':mdp', md5($row['mdp']));
        $updateStatement->bindParam(':id', $row['id']);
        $updateStatement->execute();
    }

    echo "Mots de passe hashés";

} catch (Exception $e) {
    echo "Erreur lors de l'exécution du script : " . $e->getMessage();
}
