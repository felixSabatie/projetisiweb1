<?php session_start(); ?>

<!DOCTYPE html>
<html>


<?php
require_once 'includes/php/head.php';
?>
<body>
<div class="container">
    <?php
    $activeTab = 'accueil';
    require_once 'includes/php/navBar.php';
    ?>
    <div class="row" style="padding: 10px;">
        <div class="col-sm-4">
            <img src="/includes/img/gsb1.png" class="img-rounded" alt="LOGO GSB" width="280" height="170"/>
        </div>
        <div class="col-sm-8 bg-primary">
            <h2>BIENVENUE SUR NOTRE SITE</h2>
            <h4><strong>MISSION:</strong> La qualité de nos produits et de nos services.</h4>
            <p><strong>VISION:</strong> Rendre les soins accessibles à tous ...</p>
        </div>
    </div>
</div>
<?php
require_once 'includes/php/footer.php';
?>

</body>
</html>

