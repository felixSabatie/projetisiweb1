<?php session_start(); ?>

<!DOCTYPE html>
<html>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/head.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/traitementConnexion.php');
?>

<body>

<?php
$activeTab = 'connexion';
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/navBar.php');
?>

<div class="container connexion">

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/gestion-erreur.php');
    ?>

    <h1>Connexion</h1>

    <?php
    $formAction = "seConnecter.php";
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/form_login.php');
    ?>

    <br>
    <br>

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/gestion-erreur.php');
    ?>

</div>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/footer.php');
?>

</body>
</html>

