<?php
session_start();

// Vérification sur le mois sélectionné
if(isset($_GET['mois'])
    && is_numeric($_GET['mois'])
    && floor($_GET['mois']) == $_GET['mois']
    && $_GET['mois'] <= 12
    && $_GET['mois'] > 0
) {
    $moisSelectionne = $_GET['mois'];
}
else {
    $moisSelectionne = null;
}

?>

<!DOCTYPE html>
<html>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/head.php');

// L'utilisateur doit être connecté pour accéder à la page, sinon on le redirige avec un message d'erreur
if (!isset($_SESSION['user'])) {
    $_SESSION['errorMsg'] = "Erreur, vous devez être connecté pour accéder à cette page.";
    header('Location: /seConnecter.php');
    // exit nécessaire pour que l'écriture de la variable de session soit bien prise en compte avant la redirection
    exit();
} else {
    if ($_SESSION['user']['type'] != 'comptable') {
        $_SESSION['errorMsg'] = "Erreur, seuls les comptables peuvent accéder à cette page.";
        header('Location: /seConnecter.php');
        // exit nécessaire pour que l'écriture de la variable de session soit bien prise en compte avant la redirection
        exit();
    }
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/functions.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/remboursementFiche.php');
?>

<body>

<?php
$activeTab = 'validationFicheFrais';
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/navBar.php');
?>

<div class="container validationFicheFrais">

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/gestion-erreur.php');
    ?>


    <h1>Validation des fiches de frais</h1>

    <div class="alert-container"></div>

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/select_mois.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/form_validation_frais.php');
    ?>

</div>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/footer.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/modal_structure.php');
?>

</body>
</html>
