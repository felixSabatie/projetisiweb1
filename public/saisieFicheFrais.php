<?php session_start(); ?>

<!DOCTYPE html>
<html>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/head.php');

// L'utilisateur doit être connecté pour accéder à la page, sinon on le redirige avec un message d'erreur
if (!isset($_SESSION['user'])) {
    $_SESSION['errorMsg'] = "Erreur, vous devez être connecté pour accéder à cette page.";
    header('Location: /seConnecter.php');
    // exit nécessaire pour que l'écriture de la variable de session soit bien prise en compte avant la redirection
    exit();
} else {
    if ($_SESSION['user']['type'] != 'visiteur') {
        $_SESSION['errorMsg'] = "Erreur, seuls les visiteurs peuvent accéder à cette page.";
        header('Location: /seConnecter.php');
        // exit nécessaire pour que l'écriture de la variable de session soit bien prise en compte avant la redirection
        exit();
    }
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/traitementFrais.php');
?>

<body>

<?php
$activeTab = 'saisieFicheFrais';
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/navBar.php');
?>

<div class="container saisieFicheFrais">

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/gestion-erreur.php');
    ?>


    <h1>Saisie des fiches de frais</h1>

    <div class="alert-container"></div>

    <?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/form_FF.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/form_saisie_FHF.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/table_FHF.php');
    ?>

</div>

<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/footer.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/modal_structure.php');
?>

<script type="text/javascript">

    $('.popin').on('click', '.submit', function (e) {
        e.preventDefault();

        var id = $('.popin input[name="id"]').val();
        var idVisiteur = $('.popin input[name="idVisiteur"]').val();
        var mois = $('.popin input[name="mois"]').val();

        // Elements du DOM
        var tableauContent = $('.table_fhf');
        var ligneCourante = $('.line_' + id);

        var request = $.ajax({
            type: 'POST',
            url: 'includes/ajax/delete_fhf.php',
            data: {
                id: id,
                idVisiteur: idVisiteur,
                mois: mois,
                delete_fhf: ''
            },
            dataType: 'json',
            success: function (data) {
                $('.alert-container').html('<div class="alert">'
                    + data.alertMsg
                    + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'
                    + '</div>');
                if (data.type == 'error') {
                    $('.alert-container .alert').addClass('alert-danger');
                } else {
                    $('.alert-container .alert').addClass('alert-success');
                    ligneCourante.remove();
                    if (tableauContent.find('.line').length === 0) {
                        tableauContent.after('<div class="alert alert-info">Aucune donnée à afficher pour ce mois.</div>');
                    }
                }

                $('.popin').modal('hide');
            }
        });
    });
</script>

</body>
</html>
