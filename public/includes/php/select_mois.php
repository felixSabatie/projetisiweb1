<div class="select_mois">
    <form action="/validationFicheFrais.php" method="GET">
        <label for="mois">Sélectionner un mois :</label>
        <select name="mois" class="form-control liste-mois">
            <?php
            for ($mois = 1; $mois <= 12; $mois++) {
                $date = date_create_from_format('n', $mois);
                ?>

                <option value="<?= $mois ?>"
                    <?php if (isset($moisSelectionne) && $moisSelectionne == $mois) echo 'selected' ?>
                >
                    <?= moisEnFrancais($date->format('F')) ?>
                </option>

                <?php
            }
            ?>
        </select>

        <button class="btn btn-primary">Sélectionner</button>
    </form>
</div>
