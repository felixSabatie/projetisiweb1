<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- apparemment initial-scale=1 suffit sans width -->

    <link href="includes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="includes/lib/jquery/jquery.min.js"></script>

    <!-- JavaScript Boostrap plugin -->
    <script src="includes/lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- master css -->
    <link rel="stylesheet" type="text/css" href="includes/css/master.css">

    <!-- Gestion de l'affichage de la modal au clic -->
    <script src="includes/js/gestion_modal.js"></script>

</head >
