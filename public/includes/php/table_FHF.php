<h2>Récapitulatif des frais hors forfait du mois : <?= moisEnFrancais(date('F')) ?></h2>

<?php
// Récupération des frais hors forfait du mois
try {
    $pdo = getDb();
} catch (Exception $e) {
    $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
    header('Location: /saisieFicheFrais.php');
    exit();
}
$request = "
                SELECT libelle, date, montant, mois, idVisiteur, id
                FROM LigneFraisHorsForfait
                WHERE MONTH(date) = :mois";

$statement = $pdo->prepare($request);
$statement->bindParam(':mois', date('m'));

$statement->execute();

$fraisHorsForfait = $statement->fetchAll();
?>

<div class="table_fhf">

    <table class="table">
        <thead>
        <tr class="info">
            <th>Libellé</th>
            <th>Date</th>
            <th>Montant</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <?php if (count($fraisHorsForfait) > 0): ?>

        <?php foreach ($fraisHorsForfait as $line): ?>
            <tr class="line line_<?= $line['id'] ?>">
                <td><?= $line['libelle'] ?></td>
                <td><?= $line['date'] ?></td>
                <td><p class="<?php if ($line['montant'] > 100) echo('text-danger'); ?>"><?= $line['montant'] ?></p>
                </td>
                <td>
                    <button class="btn btn-danger" data-toggle="modal"
                            data-target=".popin">Supprimer
                    </button>
                    <?php
                    require($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/modal_suppression_fhf.php');
                    ?>
                </td>
            </tr>

        <?php endforeach ?>
        </tbody>
    </table>

    <?php else: ?>
        </tbody>
        </table>
        <div class="alert alert-info">Aucune donnée à afficher pour ce mois.</div>
    <?php endif ?>

</div>

<script type="text/javascript">

    $('body').on('click', '.modal .submit',function (e) {
        e.preventDefault();

        var id = $('.modal input[name="id"]').val();
        var idVisiteur = $('.modal input[name="idVisiteur"]').val();
        var mois = $('.modal input[name="mois"]').val();

        // Elements du DOM
        var tableauContent = $('.table_fhf');
        var ligneCourante = $('.line_' + id);

        var request = $.ajax({
            type: 'POST',
            url: 'includes/ajax/delete_fhf.php',
            data: {
                id: id,
                idVisiteur: idVisiteur,
                mois: mois,
                delete_fhf: ''
            },
            dataType: 'json',
            success: function (data) {
                $('.alert-container').html('<div class="alert">'
                    + data.alertMsg
                    + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'
                    + '</div>');
                if (data.type == 'error') {
                    $('.alert-container .alert').addClass('alert-danger');
                } else {
                    $('.alert-container .alert').addClass('alert-success');
                    ligneCourante.remove();
                    if (tableauContent.find('.line').length === 0) {
                        tableauContent.after('<div class="alert alert-info">Aucune donnée à afficher pour ce mois.</div>');
                    }
                }

                $('.modal').modal('hide');
            }
        });
    });
</script>
