<?php
require_once 'includes/php/functions.php';

if(isset($_POST['username']) && isset($_POST['pwd']))
{
    try {
        $pdo = getDb();
        $request = "
                SELECT *
                FROM Visiteur
                WHERE login = :login
                AND mdp = :mdp";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':login', $_POST['username']);
        $statement->bindParam(':mdp', md5($_POST['pwd']));

        $statement->execute();

        if($row = $statement->fetch()) {
            $_SESSION['user'] = [
                'id' => $row['id'],
                'prenom' => $row['prenom'],
                'nom' => $row['nom'],
                'type' => 'visiteur',
            ];

            $_SESSION['successMsg'] = "Connexion réussie !";
            header('Location: /saisieFicheFrais.php');
            exit();
        }

        else {
            try {
                $pdo = getDb();
                $request = "
                SELECT *
                FROM Comptable
                WHERE login = :login
                AND mdp = :mdp";

                $statement = $pdo->prepare($request);
                $statement->bindParam(':login', $_POST['username']);
                $statement->bindParam(':mdp', md5($_POST['pwd']));

                $statement->execute();

                if ($row = $statement->fetch()) {
                    $_SESSION['user'] = [
                        'id' => $row['id'],
                        'prenom' => $row['prenom'],
                        'nom' => $row['nom'],
                        'type' => 'comptable',
                    ];

                    $_SESSION['successMsg'] = "Connexion réussie !";
                    header('Location: /validationFicheFrais.php?mois=' . date('n'));
                    exit();
                }
                else {
                    $_SESSION['errorMsg'] = "Le couple login/mot de passe n'est pas connu.";
                    header('Location: /seConnecter.php');
                    exit();
                }
            } catch (Exception $e) {
                $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
                header('Location: /seConnecter.php');
                exit();
            }
        }

    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
        header('Location: /seConnecter.php');
        exit();
    }
}
