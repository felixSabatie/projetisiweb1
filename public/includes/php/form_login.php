
<div class="form-container">
    <form class="form-horizontal formLogin" action="<?= $formAction ?>" method="POST" role="form">
        <div class="row">
            <label class="dark-blue col-md-4">Username</label>
            <div class="input-container col-md-8">
                <input type="text" class="form-control" placeholder="Nom d'utilisateur" name="username">
            </div>
        </div>
        
        <div class="row">
            <label class="dark-blue col-md-4">Password</label>
            <div class="input-container col-md-8">
                <input type="password" class="form-control" placeholder="Mot de passe" name="pwd">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="button-container col-md-8"><button type="submit" class="btn btn-primary submit-form">Envoyer</button></div>
        </div>

    </form>
</div>
