<div class="popin-title hidden">
    <h2>Avertissement</h2>
</div>

<div class="popin-content hidden">
    <p>Êtes-vous sûr(e) de bien vouloir supprimer cette ligne ?</p>
</div>

<div class="popin-footer hidden">
    <input type="text" class="hidden" name="idVisiteur" value="<?= $line['idVisiteur'] ?>">
    <input type="text" class="hidden" name="mois" value="<?= $line['mois'] ?>">
    <input type="text" class="hidden" name="id" value="<?= $line['id'] ?>">
    <button class="btn btn-danger submit" name="delete_fhf">Oui</button>
    <button class="btn btn-primary" data-dismiss="modal">Non</button>
</div>
