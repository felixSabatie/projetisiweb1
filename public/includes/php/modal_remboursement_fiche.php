<div class="popin-title hidden">
    <h2>Avertissement</h2>
</div>

<div class="popin-content hidden">
    <p>Êtes-vous sûr(e) de bien vouloir rembourser cette fiche de frais ?</p>
</div>

<div class="popin-footer hidden">
    <form action="/validationFicheFrais.php?mois=<?= $moisSelectionne ?>" method="POST">
        <input name="idVisiteur" type="text" class="hidden" value="<?= $fiche['visiteur'] ?>">
        <input name="mois" type="text" class="hidden" value="<?= $fiche['mois'] ?>">
        <button class="btn btn-primary submit" name="rembourser-fiche">Passer la fiche à l'état "remboursé"</button>
        <button class="btn btn-danger" data-dismiss="modal">Non</button>
    </form>
</div>
