<?php
if (isset($_POST['disconnect'])) {
    session_unset('user');
    $_SESSION['successMsg'] = "Vous avez bien été déconnecté";
    header('Location: /seConnecter.php');
    exit();
}
?>

<header>
    <div class="container">
        <nav class="navbar navbar-default bg-info">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Bouton d'affichage de la navbar sur mobile -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target=".navbar-collapse" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="accueil.php" class="navbar-brand">GSB Company</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="accueil.php" class="logo-container"><img class="logo" src="includes/img/gsb1.png"
                                                                              alt="Logo GSB"></a></li>

                        <li <?php if ($activeTab == 'accueil') echo('class="active"') ?>><a href="accueil.php">Home</a></li>

                        <?php if (isset($_SESSION['user']) && $_SESSION['user']['type'] == 'visiteur') : ?>

                            <li <?php if ($activeTab == 'saisieFicheFrais') echo('class="active"') ?>><a
                                        href="saisieFicheFrais.php">Saisie des frais médicaux</a></li>

                        <?php elseif (isset($_SESSION['user']) && $_SESSION['user']['type'] == 'comptable') : ?>

                            <li <?php if ($activeTab == 'validationFicheFrais') echo('class="active"') ?>>
                                <a href="/validationFicheFrais.php?mois=<?= date('n') ?>">Validation fiches des frais</a>
                            </li>

                        <?php endif ?>

                        <?php if (isset($_SESSION['user'])): ?>
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span>
                                    Bienvenue <?= $_SESSION['user']['prenom'] ?></a></li>

                        <?php else : ?>

                            <li <?php if ($activeTab == 'connexion') echo('class="active"') ?>><a
                                        href="seConnecter.php"><span class="glyphicon glyphicon-log-in"></span>
                                    Connexion</a>
                            </li>

                        <?php endif ?>

                    </ul>

                    <?php if (isset($_SESSION['user'])): ?>
                        <form class="navbar-left" action="seConnecter.php" method="POST">
                            <input type="hidden" name="disconnect">
                            <ul class="nav navbar-nav">
                                <li class="submit-container"><a href="#" style="cursor: pointer"
                                                                onclick="this.children[0].click();">
                                        <button class="no-style"><span
                                                    class="glyphicon glyphicon-log-out"></span> Déconnexion
                                        </button>
                                    </a>
                                </li>
                            </ul>
                        </form>
                    <?php endif ?>

                </div>
            </div>
        </nav>
    </div>
</header>
