<?php
require_once 'includes/php/functions.php';

if (isset($_POST['saisieFraisFHF'])) {
    if (isset($_POST['libelle']) && strlen($_POST['libelle']) > 0
        && isset($_POST['montant']) && strlen($_POST['montant']) > 0
    ) {
        if (!isset($_SESSION['user']['id'])) {
            $_SESSION['errorMsg'] = "Vous devez être connecté pour effectuer cette action.";
            header('Location: /seConnecter.php');
            exit();
        }

        $idVisiteur = $_SESSION['user']['id'];
        // Le mois est déterminé par les 4 chiffres de l'année et les 2 chiffres du mois
        $mois = date('Ym');
        $libelle = $_POST['libelle'];
        $date = date('Y-m-d');
        $montant = $_POST['montant'];

        try {
            $pdo = getDb();
        } catch (Exception $e) {
            $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
        }

        // Première insertion dans la table FicheFrais
        $request = "
            INSERT INTO FicheFrais (idVisiteur, mois)
            VALUES (:idVisiteur, :mois)";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);

        try {
            // Si l'insertion ne s'effectue pas correctement
            if (!$statement->execute()) {
                $_SESSION['errorMsg'] = "L'insertion a échoué";
                header('Location: /saisieFicheFrais.php');
                exit();
            }
        } catch (PDOException $e) {
            $_SESSION['errorMsg'] = "Erreur lors de l'insertion : " . $e->getMessage();
            header('Location: /saisieFicheFrais.php');
            exit();
        }

        // Puis insertion dans la table LigneFraisHorsForfait avec les clés étrangères respectées
        $request = "
            INSERT INTO LigneFraisHorsForfait (idVisiteur, mois, libelle, date, montant)
            VALUES (:idVisiteur, :mois, :libelle, :date, :montant)";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);
        $statement->bindParam(':libelle', $libelle);
        $statement->bindParam(':date', $date);
        $statement->bindParam(':montant', $montant);

        try {
            // Si l'insertion s'effectue correctement
            if ($statement->execute()) {
                $_SESSION['successMsg'] = "L'insertion a bien été effectuée";
                header('Location: /saisieFicheFrais.php');
                exit();
            } else {
                $_SESSION['errorMsg'] = "L'insertion a échoué";
                header('Location: /saisieFicheFrais.php');
                exit();
            }
        } catch (PDOException $e) {
            $_SESSION['errorMsg'] = "Erreur lors de l'insertion : " . $e->getMessage();
            header('Location: /saisieFicheFrais.php');
            exit();
        }


    } else {
        $_SESSION['errorMsg'] = "Les champs n'ont pas tous été renseignés. Merci de remplir tous les champs.";
    }
} else if (isset($_POST['saisieFF'])) {

    if (!isset($_SESSION['user']['id']) || $_SESSION['user']['type'] != 'visiteur') {
        $_SESSION['errorMsg'] = "Vous devez être connecté en tant que visiteur pour effectuer cette action.";
        header('Location: /seConnecter.php');
        exit();
    }

    try {
        $pdo = getDb();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
    }

    $idVisiteur = $_SESSION['user']['id'];
    // Le mois est déterminé par les 4 chiffres de l'année et les 2 chiffres du mois
    $mois = date('Ym');

    // On vérifie s'il existe déjà une ligne FicheFrais pour ce visiteur ce mois-ci
    $request = "
            SELECT * FROM FicheFrais
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois 
        ";

    $statement = $pdo->prepare($request);
    $statement->bindParam(':idVisiteur', $idVisiteur);
    $statement->bindParam(':mois', $mois);
    $statement->execute();

    // Sinon la ligne n'existe pas on la crée
    if (!$statement->fetch()) {
        // Première insertion dans la table FicheFrais
        $request = "
            INSERT INTO FicheFrais (idVisiteur, mois)
            VALUES (:idVisiteur, :mois)";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);

        try {
            // Si l'insertion ne s'effectue pas correctement
            if (!$statement->execute()) {
                $_SESSION['errorMsg'] = "Erreur lors de l'insertion : " . $e->getMessage();
                header('Location: /saisieFicheFrais.php');
                exit();
            }
        } catch (PDOException $e) {
            $_SESSION['errorMsg'] = "Erreur lors de l'insertion : " . $e->getMessage();
            header('Location: /saisieFicheFrais.php');
            exit();
        }
    }

    foreach ($_POST as $key => $item) {
        // On vérifie que l'item est rempli et que la clé donnée correspond bien à un FraisForfait
        if (strlen($item) > 0) {
            $request = "
                SELECT * FROM FraisForfait
                WHERE id = :id
            ";
            $statement = $pdo->prepare($request);
            $statement->bindParam(':id', $key);
            $statement->execute();

            // Si la clé existe dans les frais forfaits
            if ($statement->fetch()) {

                if(!is_numeric($item) || (is_numeric($item) && $item < 0)) {
                    $_SESSION['errorMsg'] = "Une valeur n'est pas au bon format.";
                    header('Location: /saisieFicheFrais.php');
                    exit();
                }

                $request = "
                INSERT INTO LigneFraisForfait
                VALUES (
                  :idVisiteur,
                  :mois,
                  :idFraisForfait,
                  :quantite
                )
                ";
                $statement = $pdo->prepare($request);
                $statement->bindParam(':idVisiteur', $idVisiteur);
                $statement->bindParam(':mois', $mois);
                $statement->bindParam(':idFraisForfait', $key);
                $statement->bindParam(':quantite', $item);

                try {
                    if (!$statement->execute()) {
                        $_SESSION['errorMsg'] = "Erreur lors de l'insertion";
                        header('Location: /saisieFicheFrais.php');
                        exit();
                    }
                } catch (Exception $e) {
                    $_SESSION['errorMsg'] = "Erreur lors de l'insertion : " . $e->getMessage();
                    header('Location: /saisieFicheFrais.php');
                    exit();
                }
            }
        }
    }

    $_SESSION['successMsg'] = "Les frais forfait ont bien été ajoutés.";
    header('Location: /saisieFicheFrais.php');
    exit();
}

