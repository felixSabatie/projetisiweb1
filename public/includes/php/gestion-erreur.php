<?php

if (isset($_SESSION['errorMsg'])) {
    $errorMsg = $_SESSION['errorMsg'];
    unset($_SESSION['errorMsg']);
}

if (isset($_SESSION['successMsg'])) {
    $successMsg = $_SESSION['successMsg'];
    unset($_SESSION['successMsg']);
}
?>

<?php if (isset($successMsg)): ?>
    <div class="alert alert-success">
        <?= $successMsg ?>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    </div>
<?php endif; ?>
<?php if (isset($errorMsg)): ?>
    <div class="alert alert-danger">
        <?= $errorMsg ?>
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    </div>
<?php endif; ?>
 

