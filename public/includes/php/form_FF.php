
<?php
    // Récupération des frais forfait
    try {
        $pdo = getDb();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
        header('Location: /saisieFicheFrais.php');
        exit();
    }
    $request = "
                    SELECT *
                    FROM FraisForfait";

    $statement = $pdo->prepare($request);
    $statement->execute();

    $fraisForfaits = $statement->fetchAll();
?>

<div class="frais-forfaitaires">
    <h2>Saisie des frais forfaitaires</h2>
    <hr/>

    <div class="form-saisie">
        <form action="saisieFicheFrais.php" method="post">

            <input type="text" class="hidden" name="saisieFF">

            <?php foreach($fraisForfaits as $fraisForfait): ?>
            <div class="row">
                <div class="col-md-2 libelle-saisie">
                    <?= $fraisForfait['libelle'] ?>
                </div>

                <div class="col-md-5">
                    <input name="<?= $fraisForfait['id'] ?>" class="form-control" type="number">
                </div>

                <div class="col-md-5">
                    <input class="form-control" type="text" disabled value="<?= $fraisForfait['montant'] ?>">
                </div>

            </div>
            <?php endforeach; ?>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-5"><button class="btn btn-default">Enregistrer</button></div>
                <div class="col-md-5"></div>
            </div>

        </form>
    </div>

</div>
