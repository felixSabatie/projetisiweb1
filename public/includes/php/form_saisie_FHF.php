<div class="frais-hors-forfait">
    <h2>Saisie des frais hors forfait</h2>
    <hr/>

    <div class="form-saisie">
        <form class="add_fhf" action="saisieFicheFrais.php" method="POST">

            <div class="row">
                <div class="col-md-2 libelle-saisie">
                    Libellé :
                </div>

                <div class="col-md-10">
                    <textarea class="form-control libelle" name="libelle"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 libelle-saisie">
                    Montant :
                </div>

                <div class="col-md-10">
                    <input class="form-control montant" type="number" name="montant">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <button class="btn btn-default" name="saisieFraisFHF">Enregistrer</button>
                </div>
                <div class="col-md-5"></div>
            </div>

        </form>
    </div>

</div>

<script type="text/javascript">

    // Elements du DOM
    form = $('.add_fhf');

    $(form).on('submit', function (e) {
        e.preventDefault();

        var libelle = form.find('.libelle').val();
        var montant = form.find('.montant').val();

        var request = $.ajax({
            type: 'POST',
            url: 'includes/ajax/add_fhf.php',
            data: {
                libelle: libelle,
                montant: montant,
                saisieFraisFHF: ''
            },
            dataType: 'json',
            success: function (data) {
                $('.alert-container').html('<div class="alert">'
                    + data.alertMsg
                    + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'
                    + '</div>');
                if (data.type == 'error') {
                    $('.alert-container .alert').addClass('alert-danger');
                } else {
                    $('.alert-container .alert').addClass('alert-success');
                }
            }
        });
    });
</script>
