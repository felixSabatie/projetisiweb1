<?php
if (isset($moisSelectionne)) :

    try {
        $pdo = getDb();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Impossible de se connecter à la base de données : ";
        $e->getMessage();
        header("Location: /validationFicheFrais.php");
        exit();
    }

    $date = date_create_from_format('n', $moisSelectionne);
    $moisRequete = $date->format('Ym');

    // Récupération des fiches du mois
    $request = "SELECT idVisiteur, nbJustificatifs, montantValide, idEtat
                FROM FicheFrais
                WHERE mois = :mois";
    $statement = $pdo->prepare($request);
    $statement->bindParam(":mois", $moisRequete);
    try {
        $statement->execute();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur SQL : " . $e->getMessage();
        header("Location: /validationFicheFrais.php");
        exit();
    }

    $fichesTemp = [];
    while ($row = $statement->fetch()) {
        $fiche = null;
        $fiche['etat'] = $row['idEtat'];
        $fiche['visiteur'] = $row['idVisiteur'];
        $fiche['mois'] = $moisRequete;
        $fiche['nbJustificatifs'] = $row['nbJustificatifs'];
        $fiche['montantValide'] = $row['montantValide'];
        $fiche['fraisTotaux'] = 0;

        array_push($fichesTemp, $fiche);
    }

    // Ajout de la somme des frais
    $fiches = [];
    foreach ($fichesTemp as $fiche) {
        $fraisTotaux = 0;

        // Frais forfait
        $request = "
            SELECT quantite, montant
            FROM LigneFraisForfait
            JOIN FraisForfait ON LigneFraisForfait.idFraisForfait = FraisForfait.id
            WHERE idVisiteur = :idVisiteur AND mois = :mois
        ";
        $statement = $pdo->prepare($request);
        $statement->bindParam(":idVisiteur", $fiche['visiteur']);
        $statement->bindParam(":mois", $fiche['mois']);

        try {
            $statement->execute();
        } catch (Exception $e) {
            $_SESSION['errorMsg'] = "Erreur SQL : " . $e->getMessage();
            header("Location: /validationFicheFrais.php");
            exit();
        }

        while ($row = $statement->fetch()) {
            $fraisTotaux += $row['quantite'] * $row['montant'];
        }

        // Frais hors forfait
        $request = "
            SELECT idVisiteur, mois, SUM(montant) AS total
            FROM LigneFraisHorsForfait
            WHERE idVisiteur = :idVisiteur AND mois = :mois
            GROUP BY idVisiteur, mois
        ";
        $statement = $pdo->prepare($request);
        $statement->bindParam(":idVisiteur", $fiche['visiteur']);
        $statement->bindParam(":mois", $fiche['mois']);

        try {
            $statement->execute();
        } catch (Exception $e) {
            $_SESSION['errorMsg'] = "Erreur SQL : " . $e->getMessage();
            header("Location: /validationFicheFrais.php");
            exit();
        }

        while ($row = $statement->fetch()) {
            $fraisTotaux += $row['total'];
        }

        $fiche['fraisTotaux'] = $fraisTotaux;
        array_push($fiches, $fiche);
    }

    ?>

    <div class="table_validation">

        <?php if (sizeof($fiches) > 0) : ?>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>État de la fiche</th>
                    <th>Identifiant visiteur</th>
                    <th>Mois</th>
                    <th>Nombre de justificatifs</th>
                    <th>Total validé en fonction justificatifs</th>
                    <th>Total des FF et FHF</th>
                    <th>Validation des frais et mise en paiement</th>
                    <th>Frais remboursés</th>
                </tr>
                </thead>

                <tbody>

                <?php foreach ($fiches as $fiche) : ?>

                    <tr id-visiteur="<?= $fiche['visiteur'] ?>" mois="<?= $fiche['mois'] ?>" montant-total="<?= $fiche['fraisTotaux'] ?>">
                        <td><?= $fiche['etat'] ?></td>
                        <td><?= $fiche['visiteur'] ?></td>
                        <td><?= $fiche['mois'] ?></td>
                        <td><input type="number" class="form-control nb-justificatifs"
                                   value="<?= $fiche['nbJustificatifs'] ?>"
                                <?php if ($fiche['etat'] != 'CR') echo 'disabled'; ?>
                            >
                        </td>
                        <td><input type="number" class="form-control montant-valide"
                                   value="<?= $fiche['montantValide'] ?>"
                                <?php if ($fiche['etat'] != 'CR') echo 'disabled'; ?>
                            >
                        </td>
                        <td><?= $fiche['fraisTotaux'] ?></td>
                        <td>
                            <button
                                    class="valider btn <?php if ($fiche['etat'] == 'CR') echo 'btn-primary'; else echo 'btn-danger'; ?>"
                                <?php if ($fiche['etat'] != 'CR') echo 'disabled'; else echo 'data-toggle="modal" data-target=".popin"' ?>>
                                Valider
                            </button>
                            <?php
                            // Si la fiche est validable on ajoute la modal de validation
                            if ($fiche['etat'] == 'CR') require($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/modal_validation_fiche.php');
                            ?>
                        </td>
                        <td>
                            <button
                                    class="rembourser btn <?php if ($fiche['etat'] == 'VA') echo 'btn-primary'; else echo 'btn-danger'; ?>"
                                <?php if ($fiche['etat'] != 'VA') echo 'disabled'; ?>
                                    data-toggle="modal" data-target=".popin"
                            >
                                Rembourser
                            </button>
                            <?php require($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/modal_remboursement_fiche.php'); ?>
                        </td>
                    </tr>

                <?php endforeach; ?>

                </tbody>
            </table>

            <script type="text/javascript">
                var idVisiteur;
                var nbJustificatifs;
                var montantValide;
                var mois;
                var parent;
                var montantTotal;

                // Récupération des informations au clic sur le bouton valider
                $('body').on('click', 'button.valider.btn-primary', function (e) {
                    parent = $(this.closest('tr'));
                    idVisiteur = parent.attr('id-visiteur');
                    mois = parent.attr('mois');
                    montantTotal = parent.attr('montant-total');
                    nbJustificatifs = parent.find('input.nb-justificatifs').val();
                    montantValide = parent.find('input.montant-valide').val();
                });


                $('body').on('click', '.modal .valider-fiche.submit', function (e) {
                    e.preventDefault();

                    var request = $.ajax({
                        type: 'POST',
                        url: 'includes/ajax/validation_fiche_frais.php',
                        data: {
                            idVisiteur: idVisiteur,
                            mois: mois,
                            nbJustificatifs: nbJustificatifs,
                            montantValide: montantValide,
                            montantTotal: montantTotal,
                            validerFiche: ''
                        },
                        dataType: 'json',
                        success: function (data) {
                            $('.alert-container').html('<div class="alert">'
                                + data.alertMsg
                                + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'
                                + '</div>');
                            if (data.type == 'error') {
                                console.log('Nice');
                                $('.alert-container .alert').addClass('alert-danger');
                            } else {
                                $('.alert-container .alert').addClass('alert-success');
                                parent.find('input.nb-justificatifs').attr('disabled', '');
                                parent.find('input.montant-valide').attr('disabled', '');

                                parent.find('.btn.valider').removeClass('btn-primary');
                                parent.find('.btn.valider').addClass('btn-danger');
                                parent.find('.btn.valider').attr('disabled', '');

                                parent.find('.btn.rembourser').removeClass('btn-danger');
                                parent.find('.btn.rembourser').addClass('btn-primary');
                                parent.find('.btn.rembourser').removeAttr('disabled');
                            }

                            $('.popin').modal('hide');
                        }
                    });
                });
            </script>

        <?php else : ?>
            <div class="alert alert-info">Aucune donnée à afficher pour ce mois.</div>
        <?php endif; ?>

    </div>

    <?php
endif
?>
