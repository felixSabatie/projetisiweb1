<?php

if (isset($_POST['rembourser-fiche'])) {
    if (!isset($_SESSION['user']['id']) || $_SESSION['user']['type'] != 'comptable') {
        $_SESSION['errorMsg'] = "Vous devez être connecté en tant que visiteur pour effectuer cette action.";
        header('Location: /seConnecter.php');
        exit();
    }

    if (!isset($_POST['idVisiteur']) || !isset($_POST['mois'])) {
        $_SESSION['errorMsg'] = "Erreur lors du remboursement, champs manquants.";
        header('Location: /validationFicheFrais.php');
        exit();
    }

    try {
        $pdo = getDb();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors de la connexion à la base de données";
    }

    $idVisiteur = $_POST['idVisiteur'];
    $mois = $_POST['mois'];

    // Vérification de l'état de la fiche
    $request = "
            SELECT * FROM FicheFrais
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois
            AND idEtat = 'VA'
        ";

    $statement = $pdo->prepare($request);
    $statement->bindParam(':idVisiteur', $idVisiteur);
    $statement->bindParam(':mois', $mois);
    try {
        $statement->execute();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors du remboursement, vérification de la fiche.";
        header('Location: /validationFicheFrais.php');
        exit();
    }
    if (!$row = $statement->fetch()) {
        $_SESSION['errorMsg'] = "La fiche n'est pas remboursable.";
        header('Location: /validationFicheFrais.php');
        exit();
    }

    // Update de l'état
    $request = "
            UPDATE FicheFrais
            SET dateModif = :dateModif,
            idEtat = 'CL'
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois
        ";

    $statement = $pdo->prepare($request);
    $statement->bindParam(':idVisiteur', $idVisiteur);
    $statement->bindParam(':mois', $mois);
    $statement->bindParam(':dateModif', date('Y-m-d'));
    try {
        $statement->execute();
    } catch (Exception $e) {
        $_SESSION['errorMsg'] = "Erreur lors du remboursement, udpate.";
        header('Location: /validationFicheFrais.php');
        exit();
    }

    $_SESSION['successMsg'] = "Le remboursement a bien été effectué.";
    header('Location: /validationFicheFrais.php');
    exit();
}