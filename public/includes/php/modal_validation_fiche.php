<div class="popin-title hidden">
    <h2>Avertissement</h2>
</div>

<div class="popin-content hidden">
    <p>Êtes-vous sûr(e) de bien vouloir valider cette fiche de frais ?</p>
</div>

<div class="popin-footer hidden">
    <button class="btn btn-primary submit valider-fiche" >Passer la fiche à l'état "validé"</button>
    <button class="btn btn-danger" data-dismiss="modal">Non</button>
</div>
