<?php

session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/functions.php');

if (isset($_POST['validerFiche'])) {
    if (isset($_POST['idVisiteur']) && strlen($_POST['idVisiteur']) > 0
        && isset($_POST['mois']) && strlen($_POST['mois']) > 0
        && isset($_POST['nbJustificatifs']) && strlen($_POST['nbJustificatifs']) > 0
        && isset($_POST['montantValide']) && strlen($_POST['montantValide']) > 0
    ) {
        if(!isset($_SESSION['user']['id']) || $_SESSION['user']['type'] != 'comptable') {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Vous devez être connecté en tant que comptable pour effectuer cette opération.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        // Vérification des données
        if(!is_numeric($_POST['nbJustificatifs'])  || (is_numeric($_POST['nbJustificatifs']) && $_POST['nbJustificatifs'] < 0)) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'nbJustificatifs doit être un nombre positif.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }
        if(!is_numeric($_POST['montantValide'])  || (is_numeric($_POST['montantValide']) && $_POST['montantValide'] < 0)) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'montantValide doit être un nombre positif.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        $idVisiteur = $_POST['idVisiteur'];
        $mois = $_POST['mois'];
        $nbJustificatifs = $_POST['nbJustificatifs'];
        $montantValide = $_POST['montantValide'];

        // Vérification du montant validé
        if($montantValide > $_POST['montantTotal']) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Le montant validé doit être un inférieur au montant total.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        try {
            $pdo = getDb();
        } catch (Exception $e) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Erreur lors de la connexion à la base de données.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        // Vérification de l'état de la fiche
        $request = "
            SELECT * FROM FicheFrais
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois
            AND idEtat = 'CR'
        ";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);
        try {
            $statement->execute();
        } catch (Exception $e) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Erreur lors de l\'insertion : ' . $e->getMessage()
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }
        if(!$row = $statement->fetch()) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'La fiche n\'est pas validable.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        // Update
        $request = "
            UPDATE FicheFrais
            SET nbJustificatifs = :nbJustificatifs,
            montantValide = :montantValide,
            dateModif = :dateModif,
            idEtat = 'VA'
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois
        ";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);
        $statement->bindParam(':nbJustificatifs', $nbJustificatifs);
        $statement->bindParam(':montantValide', $montantValide);
        $statement->bindParam(':dateModif', date('Y-m-d'));
        try {
            $statement->execute();
        } catch (Exception $e) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Erreur lors de l\'insertion : ' . $e->getMessage()
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        $retour = [
            'type' => 'success',
            'alertMsg' => 'La fiche a bien été validée.'
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    } else {
        $retour = [
            'type' => 'error',
            'alertMsg' => 'Merci de renseigner tous les champs.'
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    }
}

