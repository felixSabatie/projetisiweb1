<?php

session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/functions.php');

if (isset($_POST['saisieFraisFHF'])) {
    if (isset($_POST['libelle']) && strlen($_POST['libelle']) > 0
        && isset($_POST['montant']) && strlen($_POST['montant']) > 0
    ) {
        if(!isset($_SESSION['user']['id']) || $_SESSION['user']['type'] != 'visiteur') {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Vous devez être connecté en tant que visiteur pour effectuer cette opération.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        if(!is_numeric($_POST['montant'])  || (is_numeric($_POST['montant']) && $_POST['montant'] < 0)) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Montant doit être un nombre positif.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        $idVisiteur = $_SESSION['user']['id'];
        // Le mois est déterminé par les 4 chiffres de l'année et les 2 chiffres du mois
        $mois = date('Ym');
        $libelle = $_POST['libelle'];
        $date = date('Y-m-d');
        $montant = $_POST['montant'];

        try {
            $pdo = getDb();
        } catch (Exception $e) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'Erreur lors de la connexion à la base de données.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        // On vérifie s'il existe déjà une ligne FicheFrais pour ce visiteur ce mois-ci
        $request = "
            SELECT * FROM FicheFrais
            WHERE idVisiteur = :idVisiteur 
            AND mois = :mois 
        ";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);
        $statement->execute();

        // Sinon la ligne n'existe pas on la crée
        if(!$statement->fetch()) {
            // Première insertion dans la table FicheFrais
            $request = "
            INSERT INTO FicheFrais (idVisiteur, mois)
            VALUES (:idVisiteur, :mois)";

            $statement = $pdo->prepare($request);
            $statement->bindParam(':idVisiteur', $idVisiteur);
            $statement->bindParam(':mois', $mois);

            try {
                // Si l'insertion ne s'effectue pas correctement
                if(!$statement->execute()) {
                    $retour = [
                        'type' => 'error',
                        'alertMsg' => 'L\'insertion a échoué.'
                    ];
                    header('Content-Type: application/json');
                    echo json_encode($retour);
                    die();
                }
            } catch (PDOException $e) {
                $retour = [
                    'type' => 'error',
                    'alertMsg' => 'Erreur lors de l\'insertion : ' . $e->getMessage()
                ];
                header('Content-Type: application/json');
                echo json_encode($retour);
                die();
            }
        }

        // Puis insertion dans la table LigneFraisHorsForfait avec les clés étrangères respectées
        $request = "
            INSERT INTO LigneFraisHorsForfait (idVisiteur, mois, libelle, date, montant)
            VALUES (:idVisiteur, :mois, :libelle, :date, :montant)";

        $statement = $pdo->prepare($request);
        $statement->bindParam(':idVisiteur', $idVisiteur);
        $statement->bindParam(':mois', $mois);
        $statement->bindParam(':libelle', $libelle);
        $statement->bindParam(':date', $date);
        $statement->bindParam(':montant', $montant);

        try {
            // Si l'insertion s'effectue correctement
            if($statement->execute()) {
                $retour = [
                    'type' => 'success',
                    'alertMsg' => 'L\'insertion a bien été effectuée.'
                ];
                header('Content-Type: application/json');
                echo json_encode($retour);
                die();
            } else {
                $retour = [
                    'type' => 'error',
                    'alertMsg' => 'L\'insertion a échoué.'
                ];
                header('Content-Type: application/json');
                echo json_encode($retour);
                die();
            }
        } catch (PDOException $e) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'L\'insertion a échoué : ' . $e->getMessage()
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }


    } else {
        $retour = [
            'type' => 'error',
            'alertMsg' => 'Merci de renseigner tous les champs.'
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    }
}

