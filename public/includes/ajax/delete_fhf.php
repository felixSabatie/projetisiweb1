<?php

session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/' . 'includes/php/functions.php');

if(isset($_POST['delete_fhf'])) {
    if(!isset($_SESSION['user']['id'])) {
        $retour = [
            'type' => 'error',
            'alertMsg' => 'Vous devez être connecté pour effectuer cette opération.'
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    }

    try {
        $pdo = getDb();
    } catch (Exception $e) {
        $retour = [
            'type' => 'error',
            'alertMsg' => 'Erreur lors de la connexion à la base de données.'
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    }

    // Première suppression dans la table LigneFraisHorsForfait
    $request = "
            DELETE FROM LigneFraisHorsForfait
            WHERE id = :id";

    $statement = $pdo->prepare($request);
    $statement->bindParam(':id', $_POST['id']);

    try {
        // Si la suppression ne s'effectue pas correctement
        if(!$statement->execute()) {
            $retour = [
                'type' => 'error',
                'alertMsg' => 'La suppression a échoué.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }

        else {
            $retour = [
                'type' => 'success',
                'alertMsg' => 'La suppression a bien été effectuée.'
            ];
            header('Content-Type: application/json');
            echo json_encode($retour);
            die();
        }
    } catch (PDOException $e) {
        $retour = [
            'type' => 'error',
            'alertMsg' => 'Erreur lors de la suppression : ' . $e->getMessage()
        ];
        header('Content-Type: application/json');
        echo json_encode($retour);
        die();
    }
}