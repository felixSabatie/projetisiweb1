$(document).ready(function() {
    $('body').find('[data-target=".popin"]').on('click', function () {
        var popinTitle = $(this).siblings('.popin-title').html();
        $('.popin .title').html(popinTitle);

        var popinContent = $(this).siblings('.popin-content').html();
        $('.popin .modal-body').html(popinContent);

        var popinFooter = $(this).siblings('.popin-footer').html();
        $('.popin .modal-footer').html(popinFooter);
    });
});
